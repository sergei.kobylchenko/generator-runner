function worker(iter, _value, ...args) {
  const iterator = typeof iter.next === 'function' ? iter : iter(...args);

  let ret = iterator.next(_value);

  if (ret.done) {
    return ret.value;
  }

  let value = typeof ret.value === 'function' ? ret.value() : ret.value;

  if (typeof value === 'object' && typeof value.next === 'function') {
    return Promise.resolve(worker(value)).then(val => worker(iterator, val));
  }

  return Promise.resolve(value).then(val => worker(iterator, val));
}
